import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
// 导入字体图标库
import './assets/fonts/iconfont.css'
// 导入全局样式
import './assets/css/globle.css'
// 树形结构
import TreeTable from 'vue-table-with-tree-grid'
// 导入axios
import axios from 'axios'
// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'

// require styles 导入富文本编辑器对应的样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
// 将富文本编辑器注册为全局可用的组件
Vue.use(VueQuillEditor)
    // 导入Nprogress的js和css
import Nprogress from 'nprogress'
import 'nprogress/nprogress.css'
// 设置请求根路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
    // 设置请求拦截器，来为请求添加token
    // 需要授权的API， 必须在请求头中， 使用Authorization字段，提供token令牌==>权限认证
axios.interceptors.request.use(config => {
    console.log(config);
    Nprogress.start()
        // 为请求头对象， 添加token验证的Authorization
    config.headers.Authorization = window.sessionStorage.getItem('token')
    return config
})
axios.interceptors.response.use(config => {
        Nprogress.done()
        return config
    })
    // 在请求拦截器里展示进度条，在响应拦截器里隐藏进度条
    // 将axios挂载到Vue的原型上，这样全局每一个vue组件就可以通过this.$http来调用axiox
Vue.prototype.$http = axios
Vue.config.productionTip = false
    // 定义一个全局的过滤器，通过function的第一个参数，拿到需要过滤的时间
Vue.filter('dataFormate', function(oldData) {
    const dt = new Date(oldData);
    const y = dt.getFullYear();
    // 不足两位，补零，调用字符串的padStard函数(由于是字符串的函数，所以先将日期通过拼接'',转化为字符串)--
    // (第一个参数总长度为多少位，第二个参数，如果不足长度，用哪个字符串进行填充)
    const m = (dt.getMonth() + 1 + '').padStart(2, '0')
    const d = (dt.getDate() + '').padStart(2, '0')
    const hh = (dt.getHours() + '').padStart(2, '0')
    const mm = (dt.getMinutes() + '').padStart(2, '0')
    const ss = (dt.getSeconds() + '').padStart(2, '0')
        // return `yyyy-mm-dd hh:mm:ss`
    return `${y}-${m}-${d} ${hh}:${mm}:${ss}`

})
Vue.component('tree-table', TreeTable)

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')