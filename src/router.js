import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/login.vue'
import Home from './components/home.vue'


import Users from './components/user/users.vue'
import Rights from './components/power/Rights.vue'
import Roles from './components/power/Roles.vue'
import Cates from './components/goods/Cates.vue'
import Params from './components/goods/params.vue'
import List from './components/goods/List.vue'
import Add from './components/goods/Add.vue'
import Order from './components/order/Order.vue'
import Report from './components/report/Report.vue'

Vue.use(Router)
const router = new Router({
        // Router  默认是hash模式,所以在路由加载的时候，项目中的url会自带#，如果不想带#，可以改成mode：history模式
        mode: 'history',
        routes: [{
            path: '/',
            redirect: '/login'
        }, {
            path: '/login',
            component: Login
        }, {
            path: '/home',
            component: Home,
            redirect: '/users',
            children: [{
                path: '/users',
                component: Users
            }, {
                path: '/rights',
                component: Rights
            }, {
                path: '/roles',
                component: Roles
            }, {
                path: '/categories',
                component: Cates
            }, {
                path: '/params',
                component: Params
            }, {
                path: '/goods',
                component: List,

            }, {
                path: '/goods/add',
                component: Add
            }, {
                path: '/orders',
                component: Order
            }, {
                path: '/reports',
                component: Report
            }]
        }]

    })
    // 挂载路由导航守卫 
router.beforeEach((to, from, next) => {
    if (to.path === '/login') next();
    // 从sessionStroge中获取token值
    const tokenStr = window.sessionStorage.getItem("token");
    if (!tokenStr) return next('/login');
    next();
})

export default router