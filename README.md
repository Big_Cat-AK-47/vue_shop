# vue_shop

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

- 全局的属性，映射为当前的计算属性

```
如何路由守卫
如何进行异步操作，请求接口
如何保存登录的token
http拦截器


```

- 令牌机制
```
http访问需要保护的资源，需要

```
基本知识点
- router
```
- router默认是hash模式，所以项目的url地址会带#，不想要#
可以使用history模式 mode:'history';
- vue-router 路由就是跟后端交互的一种方式，通过不同的路径，请求不同的资源，
请求不同的页面，也是路由的一种功能
- $router 是 vueRouter  实例，想要导航到不同的url使用 $router.push('')
$route 为当前router 跳转对象里面可以获取name，path,query,params
- vueRouter 可以通过query和paramss进行参数传递
传递
this.$router.push('/home',params:{xx:'xx'})
this.$router.push('/home',query:{xx:'xx'})
接收
this.$route.params
this.$route.query
- params 是路由的一部分，不设置会报错 


```
- MVC  vs  MVVM
```
- MVC: 用户操作V,V将指令传递给C, C进行逻辑处理，要求M改变状态，M将新的数据发送给V,用户得到反馈 =》数据是单向的
- MVVM：采用数据的双向绑定，视图和数据没有具体联系，通过中间VM来调度
v 变化 反应在VM(M)  M变化 反应在VM(v)
###  通过双向数据绑定把 View 层和 Model 层连接了起来，而View 和 Model 之间的同步工作完全是自动的，无需人为干涉，
因此开发者只需关注业务逻辑，不需要手动操作DOM, 不需要关注数据状态的同步问题
复杂的数据状态维护完全由 MVVM 来统一管理。
- 生命周期 
- watch computed methods

当一个数据需要随着另一个数据变化时，使用computed
当一个通用的数据变化时，要执行异步的业务逻辑，建议使用watch
```
```
git stash 
git stash pop
git stah list
```
```
 - keep-alive 是vue内置的一个组件，可以使被包含的组件保留状态，避免重新渲染
```
```
vue核心
数据驱动  MVVM    组件系统 data
```


